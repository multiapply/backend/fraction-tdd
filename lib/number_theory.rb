class NumberTheory
  def self.gcd(num,denom)
    while denom != 0
      tmp = denom
      denom = num % tmp
      num = tmp
    end
    num.abs
  end
end