require_relative '../lib/number_theory'
class Fraction
  attr_reader :numerator, :denominator

  def initialize(numerator, denominator=1)
    sign_of_denominator = denominator < 0 ? -1 : 1
    gcd = NumberTheory.gcd(numerator,denominator) * sign_of_denominator

    @numerator = numerator /gcd
    @denominator = denominator /gcd
  end

  def plus(that)
    result_numerator = @numerator * that.denominator + that.numerator * @denominator
    result_denominator = @denominator * that.denominator
    Fraction.new(result_numerator, result_denominator)
  end

  def intValue()
    @numerator
  end

  def to_s
    "#{@numerator}/#{@denominator}"
  end

  def ==(that)
    return false unless that.instance_of? Fraction
    self.numerator == that.numerator and 
    self.denominator == that.denominator
  end
end