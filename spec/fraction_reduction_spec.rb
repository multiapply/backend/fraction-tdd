require_relative '../lib/fraction'
describe "Fraction Reduction" do
  it "is already in lowest terms" do
    expect(Fraction.new(3,4)).to eq Fraction.new(3,4)
  end
  
  it "reduces to not whole number" do
    expect(Fraction.new(6,8)).to eq Fraction.new(3,4)
  end

  it "reduces to whole number" do
    expect(Fraction.new(24,4)).to eq Fraction.new(6) 
  end
  
  it "reduces to zero" do
    expect(Fraction.new(0,234523)).to eq Fraction.new(0)
  end
end