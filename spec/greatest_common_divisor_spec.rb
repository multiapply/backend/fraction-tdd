require_relative '../lib/number_theory'
describe "Greatest Common Divisor" do
  it "calculates 1 for 1/1" do
    expect(NumberTheory.gcd(1,1)).to eq 1
  end

  it "calculates reflexive examples" do
    expect(NumberTheory.gcd(2,2)).to eq 2
    expect(NumberTheory.gcd(-1,-1)).to eq 1
  end

  it "calculates relatively prime" do
    expect(NumberTheory.gcd(2,3)).to eq 1
    expect(NumberTheory.gcd(4,7)).to eq 1 
    expect(NumberTheory.gcd(-2,-3)).to eq 1 
  end

  it "validates one is multiple of the other" do
    expect(NumberTheory.gcd(3,9)).to eq 3
    expect(NumberTheory.gcd(5,30)).to eq 5
  end
  
  it "is common factor" do
    expect(NumberTheory.gcd(6,8)).to eq 2
    expect(NumberTheory.gcd(49,315)).to eq 7
    expect(NumberTheory.gcd(-24,-28)).to eq 4
  end

  it "has negatives" do
    expect(NumberTheory.gcd(-24,28)).to eq 4
    expect(NumberTheory.gcd(24,-28)).to eq 4
  end

  it "has zero" do
    expect(NumberTheory.gcd(1,0)).to eq 1
    expect(NumberTheory.gcd(0,5)).to eq 5
    expect(NumberTheory.gcd(0,0)).to eq 0
  end
  
  
end

