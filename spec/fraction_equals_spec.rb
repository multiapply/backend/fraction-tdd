require_relative '../lib/fraction'
describe "Fractions Equals" do
  it "has same numerator and denominator" do
    expect(Fraction.new(3,5)).to eq Fraction.new(3,5)
  end

  it "has different numerators" do
    expect(Fraction.new(1,5)).not_to eq Fraction.new(2,5)
  end

  it "has different denominators" do
    expect(Fraction.new(3,4)).not_to eq Fraction.new(3,7)
  end
  
  it "is equal a whole number with same fraction" do
    expect(Fraction.new(5,1)).to eq Fraction.new(5)
  end

  it "is not equal a whole number with a different whole number" do
    expect(Fraction.new(6)).not_to eq Fraction.new(5)
  end
  
  it "has negative denominator" do
    expect(Fraction.new(1,2)).to eq Fraction.new(-1,-2)
    expect(Fraction.new(-1,2)).to eq Fraction.new(1,-2)
  end
end