require_relative '../lib/fraction'
describe "Fraction Addition" do
  describe "Denominator is One" do
  
    it "adds zero plus zero" do
      sum = Fraction.new(0).plus(Fraction.new(0))
      expect(sum).to eq(Fraction.new(0)) 
    end

    it "adds non-zero plus zero" do
      sum = Fraction.new(3).plus(Fraction.new(0))
      expect(sum).to eq(Fraction.new(3))
    end
    
    it "adds zero plus non-zero" do
      sum = Fraction.new(0).plus(Fraction.new(5))
      expect(sum).to eq(Fraction.new(5)) 
    end

    it "adds non-negative non-zero operands" do
      sum = Fraction.new(3).plus(Fraction.new(4))
      expect(sum).to eq(Fraction.new(7)) 
    end
    
    it "adds negative inputs and gives negative output" do
      sum = Fraction.new(-3).plus(Fraction.new(1))
      expect(sum).to eq(Fraction.new(-2)) 
    end
      
  end

  describe "Denominator is non-trivial" do
    it "adds non-negative non-zero numerators, but same denominator" do
      sum = Fraction.new(1,5).plus(Fraction.new(2,5))
      expect(sum).to eq Fraction.new(3,5)
    end

    it "adds different denominators without reducing" do
      sum = Fraction.new(1,2).plus(Fraction.new(1,3))
      expect(sum).to eq Fraction.new(5,6) 
    end
    
  end

  describe "Reduce Result" do
    it "to whole number" do
      sum = Fraction.new(1,3).plus(Fraction.new(2,3))
      expect(sum).to eq Fraction.new(1)
    end
    
    it "when one denominator is a multiple of the other" do
      sum = Fraction.new(3,4).plus(Fraction.new(5,8))
      expect(sum).to eq Fraction.new(11,8)
    end

    it "has common factor in denominators" do
      sum = Fraction.new(1,6).plus(Fraction.new(4,9))
      expect(sum).to eq Fraction.new(11,18)
    end

    it "even when denominators are the same" do
      sum = Fraction.new(3,4).plus(Fraction.new(3,4))
      expect(sum).to eq Fraction.new(3,2)
    end
    
    it "has negative fractions" do
      sum1 = Fraction.new(-1,4).plus(Fraction.new(3,4))
      expect(sum1).to eq Fraction.new(1,2)
      sum2 = Fraction.new(3,8).plus(Fraction.new(-1,2))
      expect(sum2).to eq Fraction.new(-1,8)
    end

    it "has many negative values" do
      sum = Fraction.new(1,-4).plus(Fraction.new(-3,-4))
      expect(sum).to eq Fraction.new(1,2)
    end
    
    
  end
  
end
